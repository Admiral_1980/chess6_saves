﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Chess6.Startup))]
namespace Chess6
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
