var text_notation = ("");
var text_pgn = ("");
var move_number = 1;//Номер хода
var countMove = 1;
//Координатная сетка
var index = 0;
var PozX=77, PozY=77,konX=0, konY=1;//Размеры клетки
var nachY, nachX, n1X, n2X, kletka; 
var pgnMove; var numberMove; var pgn50;
var fen = " ";
var newPozition = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
var n = 0;
var Figures_array = [];
var poleColor = [];
var newFen = [];
var poleNote = [];
var pole = [];
var currentFigure = null;
//var text_pgn = [];
var fullFen = "";
var pole = [];
for (var ip = 1; ip < 9; ip++) {
    pole[ip] = [];
    for (var jp = 1; jp < 9; jp++) {
        pole[ip][jp] = 0;
    }
}
boardSetting();
/*Динамическое создание фигуры*/
function DinFigura(Namefig, x, y, TypeFig, color, note,zif)
{
    var targetID = ConvertKoordToID(x, y);
     //korect (TypeFig);    
     this.TypeFig = TypeFig;   
     this.Namefig = Namefig;  
     this.x = x;
     this.y = y;
     this.color = color;
     this.note = note;
     this.zif = zif;
     add_figure(Namefig,TypeFig, color, targetID); 
}
//Пример объекта
var DinFigura1 = {
    Namefig: 'Вася',
    TypeFig: 'BR'
}
/*сохранение объекта
// Сохранение в local storage]
// Создадим следующий объект
var obj = { "foo": "bar", "array": [1, 2, 3] }
var newPozitionTest = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
// Сериализуем его
var sObj = JSON.stringify(obj);
var sObjFen = JSON.stringify(newPozitionTest);
// После этого sObj принимает строковое значение {"foo":"bar","array":[1,2,3]}
// Запишем в localStorage с ключём object
localStorage.setItem("object", sObj);
localStorage.setItem("savedGame1", sObjFen);
// Обратимся к localStorage следующим образом
// Хранилище вернёт нашу сериализованную строку {"foo":"bar","array":[1,2,3]}
localStorage.object;
localStorage.savedGame1;
// Получим наш сериализованный объект через API
// Одновременно преобразуем к обычному объекту JavaScript
var retObj = JSON.parse(localStorage.getItem("object"));
var loadGame1 = JSON.parse(localStorage.getItem("savedGame1"));
//alert(loadGame1);
// В итоге объекты obj и retObj абсолютно одинаковы
*/
function ConvertKoordToID(x, y) {
    var targetID = "";
    if (x == 1) {
        if (y == 1) { targetID = "Maina1"; }
        if (y == 2) { targetID = "Maina2"; }
        if (y == 3) { targetID = "Maina3"; }
        if (y == 4) { targetID = "Maina4"; }
        if (y == 5) { targetID = "Maina5"; }
        if (y == 6) { targetID = "Maina6"; }
        if (y == 7) { targetID = "Maina7"; }
        if (y == 8) { targetID = "Maina8"; }
    };
    if (x == 2) {
        if (y == 1) { targetID = "Mainb1"; }
        if (y == 2) { targetID = "Mainb2"; }
        if (y == 3) { targetID = "Mainb3"; }
        if (y == 4) { targetID = "Mainb4"; }
        if (y == 5) { targetID = "Mainb5"; }
        if (y == 6) { targetID = "Mainb6"; }
        if (y == 7) { targetID = "Mainb7"; }
        if (y == 8) { targetID = "Mainb8"; }
    }
    if (x == 3) {
        if (y == 1) { targetID = "Mainc1"; }
        if (y == 2) { targetID = "Mainc2"; }
        if (y == 3) { targetID = "Mainc3"; }
        if (y == 4) { targetID = "Mainc4"; }
        if (y == 5) { targetID = "Mainc5"; }
        if (y == 6) { targetID = "Mainc6"; }
        if (y == 7) { targetID = "Mainc7"; }
        if (y == 8) { targetID = "Mainc8"; }
    }
    if (x == 4) {
        if (y == 1) { targetID = "Maind1"; }
        if (y == 2) { targetID = "Maind2"; }
        if (y == 3) { targetID = "Maind3"; }
        if (y == 4) { targetID = "Maind4"; }
        if (y == 5) { targetID = "Maind5"; }
        if (y == 6) { targetID = "Maind6"; }
        if (y == 7) { targetID = "Maind7"; }
        if (y == 8) { targetID = "Maind8"; }
    }
    if (x == 5) {
        if (y == 1) { targetID = "Maine1"; }
        if (y == 2) { targetID = "Maine2"; }
        if (y == 3) { targetID = "Maine3"; }
        if (y == 4) { targetID = "Maine4"; }
        if (y == 5) { targetID = "Maine5"; }
        if (y == 6) { targetID = "Maine6"; }
        if (y == 7) { targetID = "Maine7"; }
        if (y == 8) { targetID = "Maine8"; }
    }
    if (x == 6) {
        if (y == 1) { targetID = "Mainf1"; }
        if (y == 2) { targetID = "Mainf2"; }
        if (y == 3) { targetID = "Mainf3"; }
        if (y == 4) { targetID = "Mainf4"; }
        if (y == 5) { targetID = "Mainf5"; }
        if (y == 6) { targetID = "Mainf6"; }
        if (y == 7) { targetID = "Mainf7"; }
        if (y == 8) { targetID = "Mainf8"; }
    }
    if (x == 7) {
        if (y == 1) { targetID = "Maing1"; }
        if (y == 2) { targetID = "Maing2"; }
        if (y == 3) { targetID = "Maing3"; }
        if (y == 4) { targetID = "Maing4"; }
        if (y == 5) { targetID = "Maing5"; }
        if (y == 6) { targetID = "Maing6"; }
        if (y == 7) { targetID = "Maing7"; }
        if (y == 8) { targetID = "Maing8"; }
    }
    if (x == 8) {
        if (y == 1) { targetID = "Mainh1"; }
        if (y == 2) { targetID = "Mainh2"; }
        if (y == 3) { targetID = "Mainh3"; }
        if (y == 4) { targetID = "Mainh4"; }
        if (y == 5) { targetID = "Mainh5"; }
        if (y == 6) { targetID = "Mainh6"; }
        if (y == 7) { targetID = "Mainh7"; }
        if (y == 8) { targetID = "Mainh8"; }
    };
    return targetID;
}
function checkField(nachX, nachY) {
    var kondX = document.getElementById("konX").value;
    var kondY = document.getElementById("konY").value;
    var nachZyfx = (nachX == "a") ? 1 : (nachX == "b") ? 2 : (nachX == "c") ? 3 : (nachX == "d") ? 4 : (nachX == "e") ? 5 : (nachX == "f") ? 6 : (nachX == "g") ? 7 : 8;
    var kondXzyf = (kondX == "a") ? 1 : (kondX == "b") ? 2 : (kondX == "c") ? 3 : (kondX == "d") ? 4 : (kondX == "e") ? 5 : (kondX == "f") ? 6 : (kondX == "g") ? 7 : 8;
    var fig = false;
    for (var iFig = 0; iFig < Figures_array.length; iFig++) {
        if (Figures_array[iFig].x == nachZyfx && Figures_array[iFig].y == nachY) //Для конкретной фигуры
        {
            fig = Figures_array[iFig];//Текущая фигура
            
            for (var jFig = 0; jFig < Figures_array.length; jFig++) {
                //Проверка на занятость клетки
                if (kondXzyf == Figures_array[jFig].x && kondY == Figures_array[jFig].y) {
                    if (fig.color == Figures_array[jFig].color) {
                        document.getElementById(fig.Namefig).style.animation = "move 2s ease"; 
                        document.getElementById(fig.Namefig).style.animationIterationCount = "1";
                        document.getElementById(fig.Namefig).style.animationFillMode = "forwards";
                        return false;
                    } else {
                        enemyFig = Figures_array[jFig];
                        takeFig(fig, enemyFig, kondXzyf, kondY);
                        return fig;
                    }
                }
            }

        }
    }
    return fig;
    
}
//Функция взятия фигуры соперника
function takeFig(fig, enemyFig, kondXzyf, kondY) {
    //TODO убрать фигуру с доски в лоток для сбитых фигур. Удалить enemyFig из массива фигур Figures_array
    var z = enemyFig.zif;
    console.log(z);
    if (z > 20) {
        var cont = document.getElementById("boxa1").textContent;
        var captureID =""; 

        if (cont == " ") {
            document.getElementById("boxa1").textContent = enemyFig.TypeFig;
            captureID = "boxa1"; index++;
            
        }
        if (cont != " ") {
            cont = document.getElementById("boxb1").textContent;
            if (cont == " ") {
                document.getElementById("boxb1").textContent = enemyFig.TypeFig;
                captureID = "boxb1"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxc1").textContent;
            if (cont == " ") {
                document.getElementById("boxc1").textContent = enemyFig.TypeFig;
                captureID = "boxc1"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxd1").textContent;
            if (cont == " ") {
                document.getElementById("boxd1").textContent = enemyFig.TypeFig;
                captureID = "boxd1"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxe1").textContent;
            if (cont == " ") {
                document.getElementById("boxe1").textContent = enemyFig.TypeFig;
                captureID = "boxe1"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxf1").textContent;
            if (cont == " ") {
                document.getElementById("boxf1").textContent = enemyFig.TypeFig;
                captureID = "boxf1"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxg1").textContent;
            if (cont == " ") {
                document.getElementById("boxg1").textContent = enemyFig.TypeFig;
                captureID = "boxg1"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxh1").textContent;
            if (cont == " ") {
                document.getElementById("boxh1").textContent = enemyFig.TypeFig;
                captureID = "boxh1"; index++;
            }
        }
    }
    if (z < 20 && z > 10)
    {
        var cont = document.getElementById("boxa2").textContent;
        if (cont == " ") {            
            document.getElementById("boxa2").textContent = enemyFig.TypeFig;
            captureID = "boxa2"; index++;
            //document.getElementById("boxa2").hidden = true; 
        }
        if (cont != " ") {
            cont = document.getElementById("boxb2").textContent;
            if (cont == " ") {
                document.getElementById("boxb2").textContent = enemyFig.TypeFig;
                captureID = "boxb2"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxc2").textContent;
            if (cont == " ") {
                document.getElementById("boxc2").textContent = enemyFig.TypeFig;
                captureID = "boxc2"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxd2").textContent;
            if (cont == " ") {
                document.getElementById("boxd2").textContent = enemyFig.TypeFig;
                captureID = "boxd2"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxe2").textContent;
            if (cont == " ") {
                document.getElementById("boxe2").textContent = enemyFig.TypeFig;
                captureID = "boxe2"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxf2").textContent;
            if (cont == " ") {
                document.getElementById("boxf2").textContent = enemyFig.TypeFig;
                captureID = "boxf2"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxg2").textContent;
            if (cont == " ") {
                document.getElementById("boxg2").textContent = enemyFig.TypeFig;
                captureID = "boxg2"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxh2").textContent;
            if (cont == " ") {
                document.getElementById("boxh2").textContent = enemyFig.TypeFig;
                captureID = "boxh2"; index++;
            }
        }
    
    }
    if (z < 2 && z > 0) {
        var cont = document.getElementById("boxa7").textContent;
        if (cont == " ") {

            document.getElementById("boxa7").textContent = enemyFig.TypeFig;
            captureID = "boxa7"; index++;
        }
        if (cont != " ") {
            cont = document.getElementById("boxb7").textContent;
            if (cont == " ") {
                document.getElementById("boxb7").textContent = enemyFig.TypeFig;
                captureID = "boxb7"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxc7").textContent;
            if (cont == " ") {
                document.getElementById("boxc7").textContent = enemyFig.TypeFig;
                captureID = "boxc7"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxd7").textContent;
            if (cont == " ") {
                document.getElementById("boxd7").textContent = enemyFig.TypeFig;
                captureID = "boxd7"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxe7").textContent;
            if (cont == " ") {
                document.getElementById("boxe7").textContent = enemyFig.TypeFig;
                captureID = "boxe7"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxf7").textContent;
            if (cont == " ") {
                document.getElementById("boxf7").textContent = enemyFig.TypeFig;
                captureID = "boxf7"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxg7").textContent;
            if (cont == " ") {
                document.getElementById("boxg7").textContent = enemyFig.TypeFig;
                captureID = "boxg7"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxh7").textContent;
            if (cont == " ") {
                document.getElementById("boxh7").textContent = enemyFig.TypeFig;
                captureID = "boxh7"; index++;
            }
        }
    }
    if (z < 10 && z > 2) {
        var cont = document.getElementById("boxa8").textContent;
        if (cont == " ") {

            document.getElementById("boxa8").textContent = enemyFig.TypeFig;
            captureID = "boxa8"; index++;
        }
        if (cont != " ") {
            cont = document.getElementById("boxb8").textContent;
            if (cont == " ") {
                document.getElementById("boxb8").textContent = enemyFig.TypeFig;
                captureID = "boxb8"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxc8").textContent;
            if (cont == " ") {
                document.getElementById("boxc8").textContent = enemyFig.TypeFig;
                captureID = "boxc8"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxd8").textContent;
            if (cont == " ") {
                document.getElementById("boxd8").textContent = enemyFig.TypeFig;
                captureID = "boxd8"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxe8").textContent;
            if (cont == " ") {
                document.getElementById("boxe8").textContent = enemyFig.TypeFig;
                captureID = "boxe8"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxf8").textContent;
            if (cont == " ") {
                document.getElementById("boxf8").textContent = enemyFig.TypeFig;
                captureID = "boxf8"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxg8").textContent;
            if (cont == " ") {
                document.getElementById("boxg8").textContent = enemyFig.TypeFig;
                captureID = "boxg8"; index++;
            }
        }
        if (cont != " ") {
            cont = document.getElementById("boxh8").textContent;
            if (cont == " ") {
                document.getElementById("boxh8").textContent = enemyFig.TypeFig;
                captureID = "boxh8"; index++;
            }
        }
    }
   
    var xbox = enemyFig.x;
    var ybox = enemyFig.y;
    enemyFig.x = "0";
    enemyFig.y = "0";
    var capturesID = document.getElementById(enemyFig.Namefig).id;
    var animeX = document.getElementById(capturesID).x;
    var animeY = document.getElementById(capturesID).y;
    
    //Замена id сбитой фигуры
    var newID = "C" + index+capturesID;
    document.getElementById(enemyFig.Namefig).id = newID;
    var targetID = ConvertKoordToID(kondXzyf, kondY);
    var koord = targetID;
    var newCid = document.getElementById(koord).children;
    for (var i = 0, child; child = newCid[i]; i++) {
        //elementChildrens - коллеция детей списка
        //child - последовательно, каждый из элементов elementChildrens
        var atributes = child.children["0"].children["0"].id;
        child.children["0"].children["0"].id = newID;
        newCid["0"].className = "c";
        atributes = child.innerHTML;
        var boxID = "#" + captureID;
        $('div.c:last').css({ 'width': '15px','height':'15px'});
        $('div.c:last').detach().prependTo(boxID);
        var box1 = document.getElementById(captureID);
        var box = getCoords(box1);
        var animeX2 = box.left;
        var animeY2 = box.top;
        animeX = animeX - animeX2;
        animeY = animeY - animeY2;
    }
    
    document.getElementById(newID).style.top = animeY+"px";
    document.getElementById(newID).style.left = animeX + "px";
    document.getElementById(newID).style.animation = "moveToBox 2s ease";
    document.getElementById(newID).style.animationIterationCount = "1";
    document.getElementById(newID).style.animationFillMode = "forwards";
}
function moveFig(fig, kondX, kondY)
{
    konX = (kondX == "a") ? 1 : (kondX == "b") ? 2 : (kondX == "c") ? 3 : (kondX == "d") ? 4 : (kondX == "e") ?
       5 : (kondX == "f") ? 6 : (kondX == "g") ? 7 : 8;
    konY = kondY;
    var noteF = fig.note;
    n1X = noteX[fig.x];   
    fig.x = konX; fig.y = konY;
    n2X = kondX;
    move(fig, konX, konY);
    createFen();  

}
// Подсветка клеток 
function light(kletka){
tochka = kletka;
pleft = podsv_left[(tochka/10|0)-1];
ptop = podsv_top[tochka%10-1];
display = "";
podsvetka(tochka, ptop, pleft, display);
}
kletka = 11; podsvetkaGreen(kletka);
kletka = 22; podsvetkaRed(kletka);
kletka = 55; podsvetkaGreen(kletka);
function podsvetkaGreen(kletka) {
    var tochka = kletka;
    switch (tochka) {
        case 11: $('#Maina1').css("background-color", "#9BFF6D"); break;
        case 12: $('#Mainb1').css("background-color", "#9BFF6D"); break;
        case 13: $('#Mainc1').css("background-color", "#9BFF6D"); break;
        case 14: $('#Maind1').css("background-color", "#9BFF6D"); break;
        case 15: $('#Maine1').css("background-color", "#9BFF6D"); break;
        case 16: $('#Mainf1').css("background-color", "#9BFF6D"); break;
        case 17: $('#Maing1').css("background-color", "#9BFF6D"); break;
        case 18: $('#Mainh1').css("background-color", "#9BFF6D"); break;
        case 21: $('#Maina2').css("background-color", "#9BFF6D"); break;
        case 22: $('#Mainb2').css("background-color", "#9BFF6D"); break;
        case 23: $('#Mainc2').css("background-color", "#9BFF6D"); break;
        case 24: $('#Maind2').css("background-color", "#9BFF6D"); break;
        case 25: $('#Maine2').css("background-color", "#9BFF6D"); break;
        case 26: $('#Mainf2').css("background-color", "#9BFF6D"); break;
        case 27: $('#Maing2').css("background-color", "#9BFF6D"); break;
        case 28: $('#Mainh2').css("background-color", "#9BFF6D"); break;
        case 31: $('#Maina3').css("background-color", "#9BFF6D"); break;
        case 32: $('#Mainb3').css("background-color", "#9BFF6D"); break;
        case 33: $('#Mainc3').css("background-color", "#9BFF6D"); break;
        case 34: $('#Maind3').css("background-color", "#9BFF6D"); break;
        case 35: $('#Maine3').css("background-color", "#9BFF6D"); break;
        case 36: $('#Mainf3').css("background-color", "#9BFF6D"); break;
        case 37: $('#Maing3').css("background-color", "#9BFF6D"); break;
        case 38: $('#Mainh3').css("background-color", "#9BFF6D"); break;
        case 41: $('#Maina4').css("background-color", "#9BFF6D"); break;
        case 42: $('#Mainb4').css("background-color", "#9BFF6D"); break;
        case 43: $('#Mainc4').css("background-color", "#9BFF6D"); break;
        case 44: $('#Maind4').css("background-color", "#9BFF6D"); break;
        case 45: $('#Maine4').css("background-color", "#9BFF6D"); break;
        case 46: $('#Mainf4').css("background-color", "#9BFF6D"); break;
        case 47: $('#Maing4').css("background-color", "#9BFF6D"); break;
        case 48: $('#Mainh4').css("background-color", "#9BFF6D"); break;
        case 51: $('#Maina5').css("background-color", "#9BFF6D"); break;
        case 52: $('#Mainb5').css("background-color", "#9BFF6D"); break;
        case 53: $('#Mainc5').css("background-color", "#9BFF6D"); break;
        case 54: $('#Maind5').css("background-color", "#9BFF6D"); break;
        case 55: $('#Maine5').css("background-color", "#9BFF6D"); break;
        case 56: $('#Mainf5').css("background-color", "#9BFF6D"); break;
        case 57: $('#Maing5').css("background-color", "#9BFF6D"); break;
        case 58: $('#Mainh5').css("background-color", "#9BFF6D"); break;
        case 61: $('#Maina6').css("background-color", "#9BFF6D"); break;
        case 62: $('#Mainb6').css("background-color", "#9BFF6D"); break;
        case 63: $('#Mainc6').css("background-color", "#9BFF6D"); break;
        case 64: $('#Maind6').css("background-color", "#9BFF6D"); break;
        case 65: $('#Maine6').css("background-color", "#9BFF6D"); break;
        case 66: $('#Mainf6').css("background-color", "#9BFF6D"); break;
        case 67: $('#Maing6').css("background-color", "#9BFF6D"); break;
        case 68: $('#Mainh6').css("background-color", "#9BFF6D"); break;
        case 71: $('#Maina7').css("background-color", "#9BFF6D"); break;
        case 72: $('#Mainb7').css("background-color", "#9BFF6D"); break;
        case 73: $('#Mainc7').css("background-color", "#9BFF6D"); break;
        case 74: $('#Maind7').css("background-color", "#9BFF6D"); break;
        case 75: $('#Maine7').css("background-color", "#9BFF6D"); break;
        case 76: $('#Mainf7').css("background-color", "#9BFF6D"); break;
        case 77: $('#Maing7').css("background-color", "#9BFF6D"); break;
        case 78: $('#Mainh7').css("background-color", "#9BFF6D"); break;
        case 81: $('#Maina8').css("background-color", "#9BFF6D"); break;
        case 82: $('#Mainb8').css("background-color", "#9BFF6D"); break;
        case 83: $('#Mainc8').css("background-color", "#9BFF6D"); break;
        case 84: $('#Maind8').css("background-color", "#9BFF6D"); break;
        case 85: $('#Maine8').css("background-color", "#9BFF6D"); break;
        case 86: $('#Mainf8').css("background-color", "#9BFF6D"); break;
        case 87: $('#Maing8').css("background-color", "#9BFF6D"); break;
        case 88: $('#Mainh8').css("background-color", "#9BFF6D"); break;

    }
    

}
function podsvetkaRed(kletka) {
    var tochka = kletka;
    switch (tochka) {
        case 11: $('#Maina1').css("background-color", "#FF8F87"); break;
        case 12: $('#Mainb1').css("background-color", "#FF8F87"); break;
        case 13: $('#Mainc1').css("background-color", "#FF8F87"); break;
        case 14: $('#Maind1').css("background-color", "#FF8F87"); break;
        case 15: $('#Maine1').css("background-color", "#FF8F87"); break;
        case 16: $('#Mainf1').css("background-color", "#FF8F87"); break;
        case 17: $('#Maing1').css("background-color", "#FF8F87"); break;
        case 18: $('#Mainh1').css("background-color", "#FF8F87"); break;
        case 21: $('#Maina2').css("background-color", "#FF8F87"); break;
        case 22: $('#Mainb2').css("background-color", "#FF8F87"); break;
        case 23: $('#Mainc2').css("background-color", "#FF8F87"); break;
        case 24: $('#Maind2').css("background-color", "#FF8F87"); break;
        case 25: $('#Maine2').css("background-color", "#FF8F87"); break;
        case 26: $('#Mainf2').css("background-color", "#FF8F87"); break;
        case 27: $('#Maing2').css("background-color", "#FF8F87"); break;
        case 28: $('#Mainh2').css("background-color", "#FF8F87"); break;
        case 31: $('#Maina3').css("background-color", "#FF8F87"); break;
        case 32: $('#Mainb3').css("background-color", "#FF8F87"); break;
        case 33: $('#Mainc3').css("background-color", "#FF8F87"); break;
        case 34: $('#Maind3').css("background-color", "#FF8F87"); break;
        case 35: $('#Maine3').css("background-color", "#FF8F87"); break;
        case 36: $('#Mainf3').css("background-color", "#FF8F87"); break;
        case 37: $('#Maing3').css("background-color", "#FF8F87"); break;
        case 38: $('#Mainh3').css("background-color", "#FF8F87"); break;
        case 41: $('#Maina4').css("background-color", "#FF8F87"); break;
        case 42: $('#Mainb4').css("background-color", "#FF8F87"); break;
        case 43: $('#Mainc4').css("background-color", "#FF8F87"); break;
        case 44: $('#Maind4').css("background-color", "#FF8F87"); break;
        case 45: $('#Maine4').css("background-color", "#FF8F87"); break;
        case 46: $('#Mainf4').css("background-color", "#FF8F87"); break;
        case 47: $('#Maing4').css("background-color", "#FF8F87"); break;
        case 48: $('#Mainh4').css("background-color", "#FF8F87"); break;
        case 51: $('#Maina5').css("background-color", "#FF8F87"); break;
        case 52: $('#Mainb5').css("background-color", "#FF8F87"); break;
        case 53: $('#Mainc5').css("background-color", "#FF8F87"); break;
        case 54: $('#Maind5').css("background-color", "#FF8F87"); break;
        case 55: $('#Maine5').css("background-color", "#FF8F87"); break;
        case 56: $('#Mainf5').css("background-color", "#FF8F87"); break;
        case 57: $('#Maing5').css("background-color", "#FF8F87"); break;
        case 58: $('#Mainh5').css("background-color", "#FF8F87"); break;
        case 61: $('#Maina6').css("background-color", "#FF8F87"); break;
        case 62: $('#Mainb6').css("background-color", "#FF8F87"); break;
        case 63: $('#Mainc6').css("background-color", "#FF8F87"); break;
        case 64: $('#Maind6').css("background-color", "#FF8F87"); break;
        case 65: $('#Maine6').css("background-color", "#FF8F87"); break;
        case 66: $('#Mainf6').css("background-color", "#FF8F87"); break;
        case 67: $('#Maing6').css("background-color", "#FF8F87"); break;
        case 68: $('#Mainh6').css("background-color", "#FF8F87"); break;
        case 71: $('#Maina7').css("background-color", "#FF8F87"); break;
        case 72: $('#Mainb7').css("background-color", "#FF8F87"); break;
        case 73: $('#Mainc7').css("background-color", "#FF8F87"); break;
        case 74: $('#Maind7').css("background-color", "#FF8F87"); break;
        case 75: $('#Maine7').css("background-color", "#FF8F87"); break;
        case 76: $('#Mainf7').css("background-color", "#FF8F87"); break;
        case 77: $('#Maing7').css("background-color", "#FF8F87"); break;
        case 78: $('#Mainh7').css("background-color", "#FF8F87"); break;
        case 81: $('#Maina8').css("background-color", "#FF8F87"); break;
        case 82: $('#Mainb8').css("background-color", "#FF8F87"); break;
        case 83: $('#Mainc8').css("background-color", "#FF8F87"); break;
        case 84: $('#Maind8').css("background-color", "#FF8F87"); break;
        case 85: $('#Maine8').css("background-color", "#FF8F87"); break;
        case 86: $('#Mainf8').css("background-color", "#FF8F87"); break;
        case 87: $('#Maing8').css("background-color", "#FF8F87"); break;
        case 88: $('#Mainh8').css("background-color", "#FF8F87"); break;

    }


}
function add_figure(Namefig,TypeFig, color,targetID) {
    var new_div = document.createElement('div');

    new_div.className = "a";
    var new_a = document.createElement('a');
    var new_image = document.createElement('img');
        new_image.className = "Figures";
   // new_image.id = ("BK");
        new_image.id = (Namefig);
   // TypeFig = 4; color = "W";
    switch (TypeFig) {
        case 1:
            switch (color) {
                case "W": new_image.src = "img/WPawn.png"; break;
                case "B": new_image.src = "img/BPawn.png"; break;
            }

            new_image.width = 53;
            new_image.height = 66; break;
        case 2:
            switch (color) {
                case "W": new_image.src = "img/WRook.png"; break;
                case "B": new_image.src = "img/BRook.png"; break;
            }
            new_image.width = 62;
            new_image.height = 70; break;
        case 3:
            switch (color) {
                case "W": new_image.src = "img/WKnight.png"; break;
                case "B": new_image.src = "img/BKnight.png"; break;
            }
            new_image.width = 75;
            new_image.height = 70; break;
        case 4:
            switch (color) {
                case "W": new_image.src = "img/WBishop.png"; break;
                case "B": new_image.src = "img/BBishop.png"; break;
            }
            new_image.width = 70;
            new_image.height = 70; break;
        case 5:
            switch (color) {
                case "W": new_image.src = "img/WQueen.png"; break;
                case "B": new_image.src = "img/BQueen.png"; break;
            }
            new_image.width = 75;
            new_image.height = 70; break;
        case 6:
            switch (color) {
                case "W": new_image.src = "img/WKing.png"; break;
                case "B": new_image.src = "img/BKing.png"; break;
            }
            new_image.width = 70;
            new_image.height = 70; break;
    }

    new_image.style = "position: inherit";    
    new_div.appendChild(new_a);
    new_a.appendChild(new_image);
    var parent = document.getElementById(targetID);   
    parent.appendChild(new_div);
}
//Добавление фигуры (при создании)
function add_figure1(Namefig, TypeFig, color,Tfig)
{
    var new_div=document.createElement('div');   
    
    new_div.className="a";    
  var new_a=document.createElement('a');
  var new_image=document.createElement('img');
    new_image.className="Figures";
   new_image.id=(Namefig);
  
    switch(TypeFig)
    {
        case 1:      
    switch(color){
        case "W": new_image.src ="img/WPawn.png";break;
     case "B": new_image.src = "img/BPawn.png"; break;
            }
    
    new_image.width=53;
    new_image.height=66;break;
    case 2:
    switch(color){
        case "W": new_image.src = "img/WRook.png"; break;
        case "B": new_image.src = "img/BRook.png"; break;
    }
    new_image.width=62;
    new_image.height=70;break;
    case 3:  
    switch(color){
        case "W": new_image.src = "img/WKnight.png"; break;
        case "B": new_image.src = "img/BKnight.png"; break;
    }
    new_image.width=79.5;
    new_image.height = 70;break; 
    case 4:
    switch(color){
        case "W": new_image.src = "~/img/WBishop.png"; break;
        case "B": new_image.src = "~/img/BBishop.png"; break;
    }
    new_image.width=79.5;
    new_image.height = 70;break;   
    case 5:
    switch(color){
        case "W": new_image.src = "~/img/WQueen.png"; break;
        case "B": new_image.src = "~/img/BQueen.png"; break;
    }
    new_image.width=79.5;
    new_image.height = 70;break;   
    case 6:
    switch(color){
        case "W": new_image.src = "~/img/WKing.png"; break;
        case "B": new_image.src = "~/img/BKing.png"; break;
    }
    new_image.width=79.5;
    new_image.height = 70;break;    
    }
                
    new_image.style =" top:" + (this.vertical)  + "px" + ";"+ "left:" + (this.horizontal) + "px;";      

    
new_div.appendChild(new_a);
new_a.appendChild(new_image);
    
document.body.insertBefore(new_div, document.body.firstChild);
}
function podsvetka(tochka, ptop, pleft, display) {
var new_div = document.createElement('div');
    new_div.id = ("light");
 var new_image=document.createElement('img');
    new_image.className ="light";
    new_image.style = "top:"+ptop+"px; left:"+pleft+"px; z-index:1";
    new_image.style.opacity = 0.3;
    new_image.style.display = display;
    new_image.id = (tochka); 
    new_image.src = "img/kletka.PNG";
    new_image.width = "77";
    new_image.height = "77";
    new_div.appendChild(new_image);
    document.body.insertBefore(new_div, document.body.firstChild);
}
var kursorX,kursorY;
window.addEventListener('click', function(e) 
{kursorX= e.pageX; kursorY= e.pageY;     
}, false); 
function a()
{       
    //document.getElementById("mouseX").value = kursorX;
    //document.getElementById("mouseY").value = kursorY;
}
  /*  1 - Пешка 2 - Ладья 3 - Конь 4 - Слон 5 - Ферзь 6 - Король  7 - Подсветка */
  //Доска с фигурами в цифровой записи
function desk()
{
    //var pole = [];
    var n = 9, m = 9;  
for (var ip = 1; ip < n; ip++)
{
    //pole[ip] = [];
    poleColor[ip] = [];
    poleNote[ip] = [];
	for (var jp = 1; jp < m; jp++)
	{
	    pole[ip][jp] = 0; poleColor[ip][jp] = ""; poleNote[ip][jp] = "";
	    for (var iFig = 0;iFig<Figures_array.length;iFig++) 
        {
            var fig = Figures_array[iFig]; 
	        if (fig.x == jp && fig.y == ip ){
	            pole[ip][jp] = fig.zif;
	            poleColor[ip][jp] = fig.color;
                poleNote[ip][jp] = fig.note;                 
            } 
        }
    }
}
//console.log(pole);

    //формирование нового имени
   // var n =1
   // var name = "WP";
   // var nameFig = name + n;
   // n++;
 $('#WsklPolea4').text(pole[3][1]);
 //$('#WsklPolea2').text(pole[2][1]);
 //$('#WsklPolea3').text(pole[3][1]);
return pole;
 
}
/* Перемещение фигуры+нотация*/    
function move(fig, konX, konY) {
     countMove++   
  TypeFig = fig.TypeFig;    
  this.konX = konX; this.konY = konY;
  this.x = konX; this.y = konY;

  var targetID = ConvertKoordToID(konX, konY);
  //this.horizontal =KoordX[this.x-1]-KorX; 
  //this.vertical = KoordY[this.y-1]-KorY;    
  var koord = targetID;
  document.getElementById(fig.Namefig).xn = koord[4];
  document.getElementById(fig.Namefig).yn = koord[5];
  //document.getElementById(fig.Namefig).style =" top:" + (this.vertical)  + "px" + ";"+ "left:" + (this.horizontal) + "px;" 
  var line_number = move_number;
  var next_line = "";
  var tochka = line_number+". "; 
  if (move_number%2 == 0)
  {   
      next_line = "\n";
      text_notation += fig.note + n1X + nachY + '-' + noteX[konX] + konY + next_line;
      text_pgn += fig.note + noteX[konX] + konY + " "; pgnMove = "w"; numberMove = move_number + 1;
      if (fig.note == "") { pgn50 = 0; } else { pgn50 += 1;}
              }  
  else {
      countMove--
      next_line = ""; 
      if (fig.note==""){var probel ="    "} else {probel ="   "}
      line_number=(line_number+1)/2;     
      text_notation += line_number + ". " + fig.note + n1X + nachY + '-' + noteX[konX] + konY + next_line + probel;
      text_pgn += line_number + ". " + fig.note + noteX[konX] + konY + " "; pgnMove = "b";
      if (fig.note == "") { pgn50 = 0; } else { pgn50 += 1; } numberMove = move_number; 
      
  } move_number++;
  
  $('#notation').text(text_notation);
  $('#pgn').text(text_pgn);
   
  createFen(); 
  fullFen += " " + pgnMove + " " + rokirovka(text_notation) + prohod(text_notation, n1X, pgnMove) + pgn50 + " " + countMove;

  //console.log(fullFen);
  desk();
  
}
function saveGame() {
// Сохранение значения

    // AInputText - входной текс 
    // AReplaceText - текст замены найденых совпадений 
    // VRegExp - экземпляр объекта RegExp (регулярное выражение)
    // VResult - результат полученный после применения регулярного выражения 
    function GetText(AInputText, AReplaceText) {
        var VRegExp = new RegExp(/\s(?=\d\.)/g);
        var VResult = AInputText.replace(VRegExp, AReplaceText);
        return VResult;
    }
    var AInputText = text_pgn;
    var AReplaceText = "\n";
    var VResult = GetText(AInputText, AReplaceText);
    text_notation = VResult;
    localStorage.setItem("Нотация игры", text_notation);
    createFen();
    localStorage.setItem("FEN", fullFen);
       alert("Игра сохранена");
   }
function loadSavedGame() {
    //  Получение значения
    alert("переменная из локал стораж" + localStorage.getItem("FEN"));
    newFen = localStorage.getItem("FEN");
    clearBoard();
    text_notation = localStorage.getItem("Нотация игры");
    $('#notation').text(text_notation);
    parceFen(newFen);
}
function deleteGame() {
    //  Удаление значения
    localStorage.removeItem("Ключ");
    alert("Игра удалена");
}
function deleteAllGames() {
    // Очистка всего хранилища
    localStorage.clear();
    alert("Все игры удалены")
}
function clearBoard() {   
    Figures_array = []; 
    //var text_notation = "";
    //var text_pgn = "";
    $("[id^=W]").remove();
    $("[id^=B]").remove();
    $('div.a').remove();
    $('#notation').text(text_notation);
    $('#pgn').text(text_pgn); 
   // newFen = newPosition; parceFen(newFen);

   // parceFen(fen); 
}
function createFen() {
    var newFen = [];
    var testlog = "";
    var count = 0; var key = 0;
    for (var j = 0; j < 64; j++) {
        key = 0;

        if (j % 8 == 0 && (j > 0)) {
            if (count > 0) { newFen.push(count); };
            newFen.push("/"); count = 0;
        };
        for (var i = 0; i < Figures_array.length; i++) {
            var testX = Figures_array[i].x == j % 8 + 1;
            var testY = Figures_array[i].y == 8 - (j / 8 | 0);
            if (Figures_array[i].x == j % 8 + 1 && Figures_array[i].y == 8 - (j / 8 | 0)) { //перебор фигур для всех клеток
                if (Figures_array[i].color == "B" && Figures_array[i].note == "R") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("r"); key = 1; break;
                };
                if (Figures_array[i].color == "B" && Figures_array[i].note == "N") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("n"); key = 1; break;
                };
                if (Figures_array[i].color == "B" && Figures_array[i].note == "B") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("b"); key = 1; break;
                };
                if (Figures_array[i].color == "B" && Figures_array[i].note == "Q") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("q"); key = 1; break;
                };
                if (Figures_array[i].color == "B" && Figures_array[i].note == "K") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("k"); key = 1; break;
                };
                if (Figures_array[i].color == "B" && Figures_array[i].note == "") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("p"); key = 1; break;
                };

                if (Figures_array[i].color == "W" && Figures_array[i].note == "R") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("R"); key = 1; break;
                };
                if (Figures_array[i].color == "W" && Figures_array[i].note == "N") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("N"); key = 1; break;
                };
                if (Figures_array[i].color == "W" && Figures_array[i].note == "B") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("B"); key = 1; break;
                };
                if (Figures_array[i].color == "W" && Figures_array[i].note == "Q") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("Q"); key = 1; break;
                };
                if (Figures_array[i].color == "W" && Figures_array[i].note == "K") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("K"); key = 1; break;
                };
                if (Figures_array[i].color == "W" && Figures_array[i].note == "") {
                    if (count > 0) { newFen.push(count); count = 0; };
                    newFen.push("P"); key = 1; break;
                };
            };//перебор фигур для всех клеток

        };//Перебор фигур (i)  
        if (key == 0) { count = count + 1; };
    };//перебор клеточек (j)
    var testlog = "";
    for (var i = 0; i < newFen.length; i++) {
        testlog += newFen[i];
        //  if (i == (newFen.length - 2)) { newFen.push(""); };           
    };
    fullFen = testlog;

    // console.log(fullFen);
    return fullFen;
}
function createFen1(poleColor, poleNote) {   
    desk();
    newFen = [];
    var poleNote = this.poleNote;
    var poleColor = this.poleColor;
    var count = 0; var key = 0;
    for (var x = 8; x > 0; x--) {
        for (var y = 1; y <= 8; y++) {

            if (poleColor[x][y] == "" && poleNote[x][y] == "") {
                count += 1;
                if (count > 8) { count = 0; }
            }
            if (poleColor[x][y] == "B" && poleNote[x][y] == "R") {
                if (count > 0) { newFen.push(count); count = 0; }
               newFen.push("r");
            }
            else if (poleColor[x][y] == "B" && poleNote[x][y] == "N") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("n");
            }
            else if (poleColor[x][y] == "B" && poleNote[x][y] == "B") {
                if (count > 0) { newFen.push(count); count = 0;}
                newFen.push("b");
            }
            else if (poleColor[x][y] == "B" && poleNote[x][y] == "N") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("n");
            }
            else if (poleColor[x][y] == "B" && poleNote[x][y] == "Q") {
                if (count > 0) { newFen.push(count); count = 0;}
                newFen.push("q");
            }
            else if (poleColor[x][y] == "B" && poleNote[x][y] == "K") {
                if (count > 0) { newFen.push(count); count = 0;}
                newFen.push("k");
            }
            else if (poleColor[x][y] == "B" && poleNote[x][y] == "") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("p");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "R") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("R");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "N") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("N");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "B") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("B");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "N") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("N");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "Q") {
                if (count > 0) { newFen.push(count); count = 0; }
                newFen.push("Q");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "K") {
                if (count > 0) { newFen.push(count); }
                newFen.push("K");
            }
            else if (poleColor[x][y] == "W" && poleNote[x][y] == "") {
                if (count > 0) {
                    newFen.push(count); count = 0;
                }
                newFen.push("P");
            }
            
           
            if (y % 8 == 0) {
                if (count > 0) { newFen.push(count); count = 0; }
                if (x != 1) {                      
                    newFen.push("/"); count = 0;
                }
            }

        }//y
    }//x
  
    var testlog = "newFen =";
    for (var i = 0; i < newFen.length; i++) {
        testlog += newFen[i];     
    }
    fullFen = testlog;
    console.log(fullFen);
    return fullFen;

}//createFen
function loadGame()
{
            /*Создание фигур*/
  var text_notation = "";
            clearBoard();
           
            parceFen(newPozition);
    desk();
    $('#WsklPolea4').text(pole[1][1]);
            createFen();         
          
            function newGame() {
              
       
                 //parceFen(newPozition);

            }          
            //createFen();
        }//loadGame
function boardSetting()
{           
  noteX = new Array("-", "a", "b", "c", "d", "e", "f", "g", "h", ":", "+", "X", "++", "!", "?");
}
        var testFen = "r3r3/PR2pk2/R2B2p1/5p2/3b1P1p/7P/4K3/8 w - - 7 38";
        //parceFen(fen);
        function parceFen(fen)
        {

            //var testFen = "2k1r3/ppp2pp1/3p1nb1/3Pr2p/1PPN3P/2R1PB2/P4KP1/4R3 w - - 1 24";
            //arFen2 : 50,107,49,114,51,47,112,112,112,50,112,112,49,47,51,112,49,110,98,49,47,51,80,114,50,
            //112, 47, 49, 80, 80, 78, 51, 80, 47, 50, 82, 49, 80, 66, 50, 47, 80, 52, 75, 80, 49, 47, 52, 82, 51, 32, 119, 32, 45, 32, 45, 32, 49, 32, 50, 52
            //var WP1fen = "8/8/8/8/8/8/P7/8 w - - KQ - 0 1"
            //Шаблон для расстановки:
            //^(.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/\S{1,8})/g
            var regexp = /(.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/.{1,8}\/\S{1,8})/g;
            var setPosition = fen.match(regexp);//Массив расстановки (FEN)    
            arFen = parseStrToBuffer(fen);
            var XPos = 0; var YPos = 8; var p = 0;
            var BRMax = 0; var BPMax = 0; var BKnMax = 0; var BBMax = 0; var BQMax = 0;
            var WRMax = 0; var WPMax = 0; var WKnMax = 0; var WBMax = 0; var WQMax = 0;
            // console.log("setPosition: " + setPosition);
            for (var i = 0; i < arFen.length; i++)
            {
                // console.log(arFen[i] + " ");
                if ((arFen[i] > 48) && (arFen[i] < 58)) {
                    p = arFen[i] - 48;
                }
                if ((arFen[i] <= 48) || (arFen[i] >= 58)) {
                    p = 1; if (arFen[i] == 47) p = 0;
                }
                if (p > 0) XPos = XPos + p;
                else { XPos = 0; YPos-- }
                if (arFen[i] < 48 || arFen[i] > 58) {
                    switch (arFen[i])
                    {
                        case 107:
                            var BK = new DinFigura("BK", XPos, YPos, 6, "B", "K", 90);
                            document.getElementById("BK").onmousedown = function () { mouseDown() };
                            document.getElementById("BK").onmouseup = function () { mouseUp() };
                            Figures_array.push(BK);
                            break;
                        case 114:
                            BRMax = BRMax + 1;
                            switch (BRMax) {
                                case 1:
                                    var BR1 = new DinFigura("BR1", XPos, YPos, 2, "B", "R", 51);
                                    document.getElementById("BR1").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR1); break;
                                case 2:
                                    var BR2 = new DinFigura("BR2", XPos, YPos, 2, "B", "R", 52);
                                    document.getElementById("BR2").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR2); break;
                                case 3:
                                    var BR3 = new DinFigura("BR3", XPos, YPos, 2, "B", "R", 53);
                                    document.getElementById("BR3").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR3); break;
                                case 4:
                                    var BR4 = new DinFigura("BR4", XPos, YPos, 2, "B", "R", 54);
                                    document.getElementById("BR4").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR4); break;
                                case 5:
                                    var BR5 = new DinFigura("BR5", XPos, YPos, 2, "B", "R", 55);
                                    document.getElementById("BR5").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR5); break;
                                case 6:
                                    var BR6 = new DinFigura("BR6", XPos, YPos, 2, "B", "R", 56);
                                    document.getElementById("BR6").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR6); break;
                                case 7:
                                    var BR7 = new DinFigura("BR7", XPos, YPos, 2, "B", "R", 57);
                                    document.getElementById("BR7").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR7); break;
                                case 8:
                                    var BR8 = new DinFigura("BR8", XPos, YPos, 2, "B", "R", 58);
                                    document.getElementById("BR8").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR8); break;
                                case 9:
                                    var BR9 = new DinFigura("BR9", XPos, YPos, 2, "B", "R", 59);
                                    document.getElementById("BR9").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR9").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR9); break;
                                case 10:
                                    var BR10 = new DinFigura("BR10", XPos, YPos, 2, "B", "R", 510);
                                    document.getElementById("BR10").onmousedown = function () { mouseDown() };
                                    document.getElementById("BR10").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BR10); break;
                                default: break;
                            };
                            break;
                        case 112:
                            BPMax = BPMax + 1;
                            switch (BPMax) {
                                case 1:
                                    var BP1 = new DinFigura("BP1", XPos, YPos, 1, "B", "", 11);
                                    document.getElementById("BP1").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP1); break;
                                case 2:
                                    var BP2 = new DinFigura("BP2", XPos, YPos, 1, "B", "", 12);
                                    document.getElementById("BP2").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP2); break;
                                case 3:
                                    var BP3 = new DinFigura("BP3", XPos, YPos, 1, "B", "", 13);
                                    document.getElementById("BP3").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP3); break;
                                case 4:
                                    var BP4 = new DinFigura("BP4", XPos, YPos, 1, "B", "", 14);
                                    document.getElementById("BP4").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP4); break;
                                case 5:
                                    var BP5 = new DinFigura("BP5", XPos, YPos, 1, "B", "", 15);
                                    document.getElementById("BP5").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP5); break;
                                case 6:
                                    var BP6 = new DinFigura("BP6", XPos, YPos, 1, "B", "", 16);
                                    document.getElementById("BP6").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP6); break;
                                case 7:
                                    var BP7 = new DinFigura("BP7", XPos, YPos, 1, "B", "", 17);
                                    document.getElementById("BP7").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP7); break;
                                case 8:
                                    var BP8 = new DinFigura("BP8", XPos, YPos, 1, "B", "", 18);
                                    document.getElementById("BP8").onmousedown = function () { mouseDown() };
                                    document.getElementById("BP8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BP8); break;

                                default: break;
                            };
                            break;
                        case 110:
                            BKnMax = BKnMax + 1;
                            switch (BKnMax) {
                                case 1:
                                    var BKn1 = new DinFigura("BKn1", XPos, YPos, 3, "B", "N", 21);
                                    document.getElementById("BKn1").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn1); break;
                                case 2:
                                    var BKn2 = new DinFigura("BKn2", XPos, YPos, 3, "B", "N", 22);
                                    document.getElementById("BKn2").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn2); break;
                                case 3:
                                    var BKn3 = new DinFigura("BKn3", XPos, YPos, 3, "B", "N", 23);
                                    document.getElementById("BKn3").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn3); break;
                                case 4:
                                    var BKn4 = new DinFigura("BKn4", XPos, YPos, 3, "B", "N", 24);
                                    document.getElementById("BKn4").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn4); break;
                                case 5:
                                    var BKn5 = new DinFigura("BKn5", XPos, YPos, 3, "B", "N", 25);
                                    document.getElementById("BKn5").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn5); break;
                                case 6:
                                    var BKn6 = new DinFigura("BKn6", XPos, YPos, 3, "B", "N", 26);
                                    document.getElementById("BKn6").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn6); break;
                                case 7:
                                    var BKn7 = new DinFigura("BKn7", XPos, YPos, 3, "B", "N", 27);
                                    document.getElementById("BKn7").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn7); break;
                                case 8:
                                    var BKn8 = new DinFigura("BKn8", XPos, YPos, 3, "B", "N", 28);
                                    document.getElementById("BKn8").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn8); break;
                                case 9:
                                    var BKn9 = new DinFigura("BKn9", XPos, YPos, 3, "B", "N", 29);
                                    document.getElementById("BKn9").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn9").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn9); break;
                                case 10:
                                    var BKn10 = new DinFigura("BKn10", XPos, YPos, 3, "B", "N", 210);
                                    document.getElementById("BKn10").onmousedown = function () { mouseDown() };
                                    document.getElementById("BKn10").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BKn10); break;
                                default: break;
                            };
                            break;
                        case 98:
                            BBMax = BBMax + 1;
                            switch (BBMax) {
                                case 1:
                                    var BB1 = new DinFigura("BB1", XPos, YPos, 4, "B", "B", 31);
                                    document.getElementById("BB1").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB1); break;
                                case 2:
                                    var BB2 = new DinFigura("BB2", XPos, YPos, 4, "B", "B", 32);
                                    document.getElementById("BB2").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB2); break;
                                case 3:
                                    var BB3 = new DinFigura("BB3", XPos, YPos, 4, "B", "B", 33);
                                    document.getElementById("BB3").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB3); break;
                                case 4:
                                    var BB4 = new DinFigura("BB4", XPos, YPos, 4, "B", "B", 34);
                                    document.getElementById("BB4").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB4); break;
                                case 5:
                                    var BB5 = new DinFigura("BB5", XPos, YPos, 4, "B", "B", 35);
                                    document.getElementById("BB5").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB5); break;
                                case 6:
                                    var BB6 = new DinFigura("BB6", XPos, YPos, 4, "B", "B", 36);
                                    document.getElementById("BB6").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB6); break;
                                case 7:
                                    var BB7 = new DinFigura("BB7", XPos, YPos, 4, "B", "B", 37);
                                    document.getElementById("BB7").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB7); break;
                                case 8:
                                    var BB8 = new DinFigura("BB8", XPos, YPos, 4, "B", "B", 38);
                                    document.getElementById("BB8").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB8); break;
                                case 9:
                                    var BB9 = new DinFigura("BB9", XPos, YPos, 4, "B", "B", 39);
                                    document.getElementById("BB9").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB9").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB9); break;
                                case 10:
                                    var BB10 = new DinFigura("BB10", XPos, YPos, 4, "B", "B", 310);
                                    document.getElementById("BB10").onmousedown = function () { mouseDown() };
                                    document.getElementById("BB10").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BB10); break;
                                default: break;
                            };
                            break;
                        case 80:
                            WPMax = WPMax + 1;
                            switch (WPMax) {
                                case 1:
                                    var WP1 = new DinFigura("WP1", XPos, YPos, 1, "W", "", 1.1);
                                    document.getElementById("WP1").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP1); break;
                                case 2:
                                    var WP2 = new DinFigura("WP2", XPos, YPos, 1, "W", "", 1.2);
                                    document.getElementById("WP2").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP2); break;
                                case 3:
                                    var WP3 = new DinFigura("WP3", XPos, YPos, 1, "W", "", 1.3);
                                    document.getElementById("WP3").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP3); break;
                                case 4:
                                    var WP4 = new DinFigura("WP4", XPos, YPos, 1, "W", "", 1.4);
                                    document.getElementById("WP4").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP4); break;
                                case 5:
                                    var WP5 = new DinFigura("WP5", XPos, YPos, 1, "W", "", 1.5);
                                    document.getElementById("WP5").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP5); break;
                                case 6:
                                    var WP6 = new DinFigura("WP6", XPos, YPos, 1, "W", "", 1.6);
                                    document.getElementById("WP6").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP6); break;
                                case 7:
                                    var WP7 = new DinFigura("WP7", XPos, YPos, 1, "W", "", 1.7);
                                    document.getElementById("WP7").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP7); break;
                                case 8:
                                    var WP8 = new DinFigura("WP8", XPos, YPos, 1, "W", "", 1.8);
                                    document.getElementById("WP8").onmousedown = function () { mouseDown() };
                                    document.getElementById("WP8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WP8); break;

                                default: break;
                            };
                            break;
                        case 78:
                            WKnMax = WKnMax + 1;
                            switch (WKnMax) {
                                case 1:
                                    var WKn1 = new DinFigura("WKn1", XPos, YPos, 3, "W", "N", 2.1);
                                    document.getElementById("WKn1").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn1); break;
                                case 2:
                                    var WKn2 = new DinFigura("WKn2", XPos, YPos, 3, "W", "N", 2.2);
                                    document.getElementById("WKn2").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn2); break;
                                case 3:
                                    var WKn3 = new DinFigura("WKn3", XPos, YPos, 3, "W", "N", 2.3);
                                    document.getElementById("WKn3").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn3); break;
                                case 4:
                                    var WKn4 = new DinFigura("WKn4", XPos, YPos, 3, "W", "N", 2.4);
                                    document.getElementById("WKn4").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn4); break;
                                case 5:
                                    var WKn5 = new DinFigura("WKn5", XPos, YPos, 3, "W", "N", 2.5);
                                    document.getElementById("WKn5").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn5); break;
                                case 6:
                                    var WKn6 = new DinFigura("WKn6", XPos, YPos, 3, "W", "N", 2.6);
                                    document.getElementById("WKn6").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn6); break;
                                case 7:
                                    var WKn7 = new DinFigura("WKn7", XPos, YPos, 3, "W", "N", 2.7);
                                    document.getElementById("WKn7").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn7); break;
                                case 8:
                                    var WKn8 = new DinFigura("WKn8", XPos, YPos, 3, "W", "N", 2.8);
                                    document.getElementById("WKn8").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn8); break;
                                case 9:
                                    var WKn9 = new DinFigura("WKn9", XPos, YPos, 3, "W", "N", 2.9);
                                    document.getElementById("WKn9").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn9").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn9); break;
                                case 10:
                                    var WKn10 = new DinFigura("WKn10", XPos, YPos, 3, "W", "N", 2.10);
                                    document.getElementById("WKn10").onmousedown = function () { mouseDown() };
                                    document.getElementById("WKn10").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WKn10); break;
                                default: break;
                            };
                            break;
                        case 113:
                            BQMax = BQMax + 1;
                            switch (BQMax) {
                                case 1:
                                    var BQ = new DinFigura("BQ", XPos, YPos, 5, "B", "Q", 80);
                                    document.getElementById("BQ").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ); break;
                                case 2:
                                    var BQ1 = new DinFigura("BQ1", XPos, YPos, 5, "B", "Q", 81);
                                    document.getElementById("BQ1").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ1); break;
                                case 3:
                                    var BQ2 = new DinFigura("BQ2", XPos, YPos, 5, "B", "Q", 82);
                                    document.getElementById("BQ2").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ2); break;
                                case 4:
                                    var BQ3 = new DinFigura("BQ3", XPos, YPos, 5, "B", "Q", 83);
                                    document.getElementById("BQ3").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ3); break;
                                case 5:
                                    var BQ4 = new DinFigura("BQ4", XPos, YPos, 5, "B", "Q", 84);
                                    document.getElementById("BQ4").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ4); break;
                                case 6:
                                    var BQ5 = new DinFigura("BQ5", XPos, YPos, 5, "B", "Q", 85);
                                    document.getElementById("BQ5").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ5); break;
                                case 7:
                                    var BQ6 = new DinFigura("BQ6", XPos, YPos, 5, "B", "Q", 86);
                                    document.getElementById("BQ6").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ6); break;
                                case 8:
                                    var BQ7 = new DinFigura("BQ7", XPos, YPos, 5, "B", "Q", 87);
                                    document.getElementById("BQ7").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ7); break;
                                case 9:
                                    var BQ8 = new DinFigura("BQ8", XPos, YPos, 5, "B", "Q", 88);
                                    document.getElementById("BQ8").onmousedown = function () { mouseDown() };
                                    document.getElementById("BQ8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(BQ8); break;

                                default: break;
                            };
                            break;
                        case 81:
                            WQMax = WQMax + 1;
                            switch (WQMax)
                            {
                                case 1:
                                    var WQ = new DinFigura("WQ", XPos, YPos, 5, "W", "Q", 8);
                                    document.getElementById("WQ").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ); break;
                                case 2:
                                    var WQ1 = new DinFigura("WQ1", XPos, YPos, 5, "W", "Q", 8.1);
                                    document.getElementById("WQ1").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ1); break;
                                case 3:
                                    var WQ2 = new DinFigura("WQ2", XPos, YPos, 5, "W", "Q", 8.2);
                                    document.getElementById("WQ2").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ2); break;
                                case 4:
                                    var WQ3 = new DinFigura("WQ3", XPos, YPos, 5, "W", "Q", 8.3);
                                    document.getElementById("WQ3").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ3); break;
                                case 5:
                                    var WQ4 = new DinFigura("WQ4", XPos, YPos, 5, "W", "Q", 8.4);
                                    document.getElementById("WQ4").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ4); break;
                                case 6:
                                    var WQ5 = new DinFigura("WQ5", XPos, YPos, 5, "W", "Q", 8.5);
                                    document.getElementById("WQ5").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ5); break;
                                case 7:
                                    var WQ6 = new DinFigura("WQ6", XPos, YPos, 5, "W", "Q", 8.6);
                                    document.getElementById("WQ6").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ6); break;
                                case 8:
                                    var WQ7 = new DinFigura("WQ7", XPos, YPos, 5, "W", "Q", 8.7);
                                    document.getElementById("WQ7").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ7); break;
                                case 9:
                                    var WQ8 = new DinFigura("WQ8", XPos, YPos, 5, "W", "Q", 8.8);
                                    document.getElementById("WQ8").onmousedown = function () { mouseDown() };
                                    document.getElementById("WQ8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WQ8); break;

                                default: break;
                            };
                            break;
                        case 82:
                            WRMax = WRMax + 1;
                            switch (WRMax)
                            {
                                case 1:
                                    var WR1 = new DinFigura("WR1", XPos, YPos, 2, "W", "R", 5.1);
                                    document.getElementById("WR1").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR1); break;
                                case 2:
                                    var WR2 = new DinFigura("WR2", XPos, YPos, 2, "W", "R", 5.2);
                                    document.getElementById("WR2").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR2); break;
                                case 3:
                                    var WR3 = new DinFigura("WR3", XPos, YPos, 2, "W", "R", 5.3);
                                    document.getElementById("WR3").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR3); break;
                                case 4:
                                    var WR4 = new DinFigura("WR4", XPos, YPos, 2, "W", "R", 5.4);
                                    document.getElementById("WR4").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR4); break;
                                case 5:
                                    var WR5 = new DinFigura("WR5", XPos, YPos, 2, "W", "R", 5.5);
                                    document.getElementById("WR5").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR5); break;
                                case 6:
                                    var WR6 = new DinFigura("WR6", XPos, YPos, 2, "W", "R", 5.6);
                                    document.getElementById("WR6").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR6); break;
                                case 7:
                                    var WR7 = new DinFigura("WR7", XPos, YPos, 2, "W", "R", 5.7);
                                    document.getElementById("WR7").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR7); break;
                                case 8:
                                    var WR8 = new DinFigura("WR8", XPos, YPos, 2, "W", "R", 5.8);
                                    document.getElementById("WR8").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR8); break;
                                case 9:
                                    var WR9 = new DinFigura("WR9", XPos, YPos, 2, "W", "R", 5.9);
                                    document.getElementById("WR9").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR9").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR9); break;
                                case 10:
                                    var WR10 = new DinFigura("WR10", XPos, YPos, 2, "W", "R", 5.10);
                                    document.getElementById("WR10").onmousedown = function () { mouseDown() };
                                    document.getElementById("WR10").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WR10); break;
                                default: break;
                            };
                            break;
                        case 66:
                            WBMax = WBMax + 1;
                            switch (WBMax)
                            {
                                case 1:
                                    var WB1 = new DinFigura("WB1", XPos, YPos, 4, "W", "B", 3.1);
                                    document.getElementById("WB1").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB1").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB1); break;
                                case 2:
                                    var WB2 = new DinFigura("WB2", XPos, YPos, 4, "W", "B", 3.2);
                                    document.getElementById("WB2").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB2").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB2); break;
                                case 3:
                                    var WB3 = new DinFigura("WB3", XPos, YPos, 4, "W", "B", 3.3);
                                    document.getElementById("WB3").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB3").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB3); break;
                                case 4:
                                    var WB4 = new DinFigura("WB4", XPos, YPos, 4, "W", "B", 3.4);
                                    document.getElementById("WB4").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB4").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB4); break;
                                case 5:
                                    var WB5 = new DinFigura("WB5", XPos, YPos, 4, "W", "B", 3.5);
                                    document.getElementById("WB5").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB5").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB5); break;
                                case 6:
                                    var WB6 = new DinFigura("WB6", XPos, YPos, 4, "W", "B", 3.6);
                                    document.getElementById("WB6").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB6").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB6); break;
                                case 7:
                                    var WB7 = new DinFigura("WB7", XPos, YPos, 4, "W", "B", 3.7);
                                    document.getElementById("WB7").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB7").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB7); break;
                                case 8:
                                    var WB8 = new DinFigura("WB8", XPos, YPos, 4, "W", "B", 3.8);
                                    document.getElementById("WB8").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB8").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB8); break;
                                case 9:
                                    var WB9 = new DinFigura("BB9", XPos, YPos, 4, "W", "B", 3.9);
                                    document.getElementById("WB9").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB9").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB9); break;
                                case 10:
                                    var WB10 = new DinFigura("WB10", XPos, YPos, 4, "W", "B", 3.10);
                                    document.getElementById("WB10").onmousedown = function () { mouseDown() };
                                    document.getElementById("WB10").onmouseup = function () { mouseUp() };
                                    Figures_array.push(WB10); break;
                                default: break;
                            };
                            break;
                        case 75:
                            var WK = new DinFigura("WK", XPos, YPos, 6, "W", "K", 9);
                            document.getElementById("WK").onmousedown = function () { mouseDown() };
                            document.getElementById("WK").onmouseup = function () { mouseUp() };
                            Figures_array.push(WK);
                            break;
                        default: break;

                    }
                }
                if (arFen[i] == 32) break;
            }
            $(function () {
                $("[id^=W]").draggable({ containment: [0, 50, 1280, 1280]});
                $("[id^=B]").draggable({ containment: [0, 50, 1280,1280] });
            })
            CorrectByNotation();
        }
function CorrectByNotation() {

}
function mouseDown()
{
   //Фигура взята
        $('.Figures').mousedown(function () {
            var koords = $(this).parents();
            currentFigure = this.id;
            var koord = koords[2].id;
            var nachX = koord[4];
            var nachY = koord[5];
            document.getElementById("mouseX").value = nachX;
            document.getElementById("mouseY").value = nachY; 

        });

}
function mouseUp()
{
   //Фигуру отпустили
  
    $(function ()
    {
        var flag = 0;
        $(".black").droppable(
            {
                drop: function (event, ui)
                {
                    var koords = $(this).attr('id');
                     var kondX = koords[4];
                     var kondY = koords[5];
                     document.getElementById("konX").value = kondX;
                     document.getElementById("konY").value = kondY;
                     nachX = document.getElementById("mouseX").value;
                     nachY = document.getElementById("mouseY").value;
                    // console.log(kondX + kondY);
                     fig = checkField(nachX, nachY);
                     if (fig) {
                         moveFig(fig, kondX, kondY);
                         newPozition = fullFen;
                         if (fig.x !== 0) {
                             clearBoard();
                         }
                         parceFen(newPozition);                        
                         desk(); key = 1;                       
                     }
                    flag = 1;
                    currentFigure = null;                    
                }

            }); 
        $(".white").droppable(
            {
                drop: function (event, ui) {
                    var koords = $(this).attr('id');
                     var kondX = koords[4];
                     var kondY = koords[5];
                     document.getElementById("konX").value = kondX;
                     document.getElementById("konY").value = kondY;
                     nachX = document.getElementById("mouseX").value;
                     nachY = document.getElementById("mouseY").value;
                     fig = checkField(nachX, nachY);
                     if (fig) {                         
                         moveFig(fig, kondX, kondY);
                         newPozition = fullFen;
                         if (fig.x !== 0) {
                             clearBoard();
                         }
                         parceFen(newPozition);
                         desk();                         
                    }
                    currentFigure = null;
                    flag = 1;
                }
            }); 
        if (flag == 0 && currentFigure != null) {
            document.getElementById(currentFigure).style.animation = "move 2s ease";
            document.getElementById(currentFigure).style.animationIterationCount = "1";
            document.getElementById(currentFigure).style.animationFillMode = "forwards";
            currentFigure = null;

        }
    });  


}
function fenGen() {
    var newPozition = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    console.log(newPozition);
}
function checkSlash(i, arFen, count) {

    if (arFen[i] == 47) {
        count++;
        console.log("/");
        //            arFen[i] = ("/");
    };
    return count;
}
var n = 8, m = 8;
var mas = [];
for (var i = 0; i < m; i++){
	mas[i] = [];
	for (var j = 0; j < n; j++){
		mas[i][j] = 0;
}}
//$('#WsklPolea1').text(pole[1][1]);
function parseStrToBuffer(string) {
    var result = [],
    index = 0,
    length = string.length,
    code;

    for (; index < length; index++) {
        code = string.charCodeAt(index);
        if (code <= 0x7f) {
            result.push(code);
        } else if (code <= 0x7ff) {
            result.push(code >>> 6 | 0xc0,
              code & 0x3f | 0x80);
        } else if (code <= 0xffff) {
            result.push(code >>> 12 | 0xe0,
              code >>> 6 & 0x3f | 0x80,
              code & 0x3f | 0x80);
        } else if (code <= 0x1fffff) {
            result.push(code >>> 18 | 0xf0,
              code >>> 12 & 0x3f | 0x80,
              code >>> 6 & 0x3f | 0x80,
              code & 0x3f | 0x80);
        } else if (code <= 0x3ffffff) {
            result.push(code >>> 24 | 0xf8,
              code >>> 18 & 0x3f | 0x80,
              code >>> 12 & 0x3f | 0x80,
              code >>> 6 & 0x3f | 0x80,
              code & 0x3f | 0x80);
        } else if (code <= 0x7fffffff) {
            result.push(code >>> 30 | 0xfc,
              code >>> 24 & 0x3f | 0x80,
              code >>> 18 & 0x3f | 0x80,
              code >>> 12 & 0x3f | 0x80,
              code >>> 6 & 0x3f | 0x80,
              code & 0x3f | 0x80);
        };
    };

    return result;
};
// Test string: ﻰﺠ﷼ﺒ╤Ή
// Result: [ 239, 187, 176, 239, 186, 160, 239, 183, 188, 239, 186, 146, 226, 149, 164, 206, 137 ]
//alert(JSON.stringify(parseStrToBuffer(prompt('Enter string'))));
// Проверка возможности рокировки
function rokirovka(text_notation,testlog) {
    var k = ""; var K = ""; var q = ""; var Q = ""; var kq = ""; var KQ = "";
    var regexp = /(Ra1)/;
    var regexp1 = /(Ke1)/;
    Q = text_notation.match(regexp);
    KQ = text_notation.match(regexp1);
    if (KQ == null) { KQ = ""; }
    if (Q == null) { Q = ""; }
    if (Q == KQ) { Q = "Q"; } else if (KQ[0] == "Ke1") { Q = ""; }
    else if (Q[0] == "Ra1") { Q = ""; }
    regexp = /(Rh1)/;
    K = text_notation.match(regexp);
    if (KQ[0] == "Ke1") { K = ""; }
    if (K == null) { K = "K"; } else { K = ""; }

    var regexp = /(Ra8)/;
    var regexp1 = /(Ke8)/;
    q = text_notation.match(regexp);
    kq = text_notation.match(regexp1);
    if (kq == null) { kq = ""; }
    if (q == null) { q = ""; }
    if (q == kq) { q = "q"; } else if (kq[0] == "Ke8") { q = ""; }
    else if (q[0] == "Ra8") { q = ""; }
    regexp = /(Rh8)/;
    k = text_notation.match(regexp);
    if (kq[0] == "Ke8") { k = ""; }
    if (k == null) { k = "k"; } else { k = ""; }
    var rokFen = K + Q + k + q;
    return rokFen;
}
function prohod(text_notation,n1X,pgnMove) {
    var regexp = /\W.2-(.)4....$/g;
    var regexp1 = /\W.7-(.)5\n$/g;
    //var regexp1 = /\W.7-(.)5/g;
    if (pgnMove == "b") {
        var proFen = text_notation.match(regexp);
        if (proFen == null) { proFen = " - "; }
        else {
            proFen = " " + n1X + "3 ";
        }
    } else
        if (pgnMove == "w") {
            proFen = text_notation.match(regexp1);
            if (proFen == null) { proFen = " - "; }
            else {
               proFen = " " + n1X + "6 "; 
            }
        }
    return proFen;
}
//Получение координат элемента
function getCoords(elem) { // кроме IE8-
    var box = elem.getBoundingClientRect();

    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    };

};
//// пример
//var box = document.getElementById("box1");
//var WQ = document.getElementById("WQ");
////----------------------------
////получаем координаты ферзя:
//WQ.coord= getCoords(WQ);
//alert("WQ top: " +  WQ.coord.top);
//alert(WQ.coord.left);
////-------------------------
////аналогично координаты лотка:
//box.coord= getCoords(box);
//alert(box.coord.top);
//alert(box.coord.left);   